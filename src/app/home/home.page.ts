import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  private genders = [];
  private times = [];
  private bottles = [];
  private bottle: number;
  private time: number;
  private weight: number;
  private gender: string;
  private promilles: number;
  private litres: number;
  private grams: number;
  private burning: number;
  private gramsLeft: number;

  constructor() {}
ngOnInit() {
    this.genders.push('Male');
    this.genders.push('Female');

    this.times.push('1');
    this.times.push('2');
    this.times.push('3');
    this.times.push('4');
    this.times.push('5');

    this.bottles.push('1');
    this.bottles.push('2');
    this.bottles.push('3');
    this.bottles.push('4');
    this.bottles.push('5');

    this.gender = 'Male';



  }
  private calculate() {
    let factor = 0;
    let factor2 = 0;

    switch (this.time) {
      case 1:
        factor = 1;
        break;
      case 2:
         factor = 2;
         break;
       case 3:
         factor = 3;
         break;
       case 4:
         factor = 4;
         break;
        case 5:
           factor = 5;
           break;
    }
    switch (this.bottle) {
      case 1:
        factor2 = 1;
        break;
      case 2:
         factor2 = 2;
         break;
       case 3:
         factor2 = 3;
         break;
       case 4:
         factor2 = 4;
         break;
        case 5:
           factor2 = 5;
           break;
    }
    this.litres = (this.bottle * 0.33);
    this.grams = (this.litres * 8 * 4.5);
    this.burning = (this.weight / 10);
    this.gramsLeft = ( this.grams - (this.burning * this.time));


    if (this.gender === 'Male'){
      this.promilles = this.grams / (this.weight * 0.7);
    }
    else {
      this.promilles = this.grams / (this.weight * 0.6);
    }
}
}
